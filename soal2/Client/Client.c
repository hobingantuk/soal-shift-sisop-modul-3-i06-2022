#include "stdio.h"
#include "errno.h"
#include "netdb.h"
#include "unistd.h" 
#include "string.h"
#include "strings.h"
#include "netinet/in.h"
#include "sys/stat.h"
#define PORT 8080


void recvWithoutTestByte(int fd, char *buffer, size_t size)
{
	int byteCode = '0';
	while(byteCode == '0')
	{
		recv(fd, buffer, size, 0);
		byteCode = buffer[0];
	}
}
	


int main()
{
	struct sockaddr_in socketAddress;
	int socketFileDescriptor;
	struct hostent *serverHostData;
	char message[2048];
	
	socketFileDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (socketFileDescriptor == -1)
	{
		fprintf(stderr, "Socket failed: %s\n", strerror(errno));
		return -1;
	}
	
	serverHostData = gethostbyname("localhost");
	socketAddress.sin_family = AF_INET;
	socketAddress.sin_port = htons(PORT);
	socketAddress.sin_addr = *((struct in_addr *)serverHostData->h_addr);
	
	if (connect(socketFileDescriptor, (struct sockaddr *)&socketAddress, sizeof(struct sockaddr_in)) == -1)
	{
		fprintf(stderr, "Socket connection failed: %s\n", strerror(errno));
		close(socketFileDescriptor);
		return -1;
	}
	
	int currPage = 0;
	int response = 0;
	char username[33];
	char password[33];
	char command[2][256];
	
	while(1)
	{
		response = 0;
		
		
		
		if (currPage == 0)
		{
			while(response <= 0 || response >= 3)
			{
				printf("Pilih menu\n");
				printf("1. Register\n");
				printf("2. Login\n");
				scanf("%d", &response);	
			}
			
			if (response == 1)
			{
				currPage = 1;
			}
			else if (response == 2)
			{
				currPage = 2;
			}
		}
		else if (currPage == 1)
		{
			printf("Register\n");
			printf("Username dan password max length is 32\n");
			printf("Password must contain capital, small letter, and numeric\n");
			printf("Username: ");
			scanf("%s", username);
			printf("Password: ");
			scanf("%s", password);
			
			message[0] = 'r';
			message[1] = strlen(username);
			message[2] = strlen(password);
			message[3] = '\0';
			strcat(message, username);
			strcat(message, password);
			
			send(socketFileDescriptor, message, sizeof(message), 0);
			
			recvWithoutTestByte(socketFileDescriptor, message, sizeof(message));
			
			if (message[0] == 'd')
			{
				printf("\nRegister Failed\n\n");
			}
			currPage = 0;
		}
		else if (currPage == 2)
		{
			
			printf("\nLogin\n");
			printf("Username: ");
			scanf("%s", username);
			printf("Password: ");
			scanf("%s", password);
			
			message[0] = 'l';
			message[1] = strlen(username);
			message[2] = strlen(password);
			message[3] = '\0';
			strcat(message, username);
			strcat(message, password);
			
			send(socketFileDescriptor, message, sizeof(message), 0);
			
			recvWithoutTestByte(socketFileDescriptor, message, sizeof(message));
			
			if (message[0] == 'l')
			{
				currPage = 3;
			}
			else if (message[0] == 'd')
			{
				printf("\nLogin Failed\n\n");
				currPage = 0;
			}
			else if (message[0] == 'u')
			{
				printf("\nAnother user still login\n\n");
				currPage = 0;
			}
		}
	}
	return 0;
}
