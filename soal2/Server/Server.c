#include "stdio.h"
#include "unistd.h" 
#include "string.h"
#include "strings.h"
#include "errno.h"
#include "netinet/in.h" 
#include "sys/epoll.h"
#include "sys/types.h"
#include "sys/stat.h"
#define PORT 8080

int createServerSocket() 
{
    struct sockaddr_in socketAddress;
    int socketFileDescriptor;

    socketFileDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); 
    if (socketFileDescriptor == -1) {
        fprintf(stderr, "socket failed: %s\n", strerror(errno));
        return -1;
    }

    socketAddress.sin_family = AF_INET;         
    socketAddress.sin_port = htons(PORT);     
    socketAddress.sin_addr.s_addr = INADDR_ANY; 

    if (bind(socketFileDescriptor, (struct sockaddr *)&socketAddress, sizeof(struct sockaddr_in)) != 0) {
        fprintf(stderr, "bind failed: %s\n", strerror(errno));
        close(socketFileDescriptor);
        return -1;
    }

    if (listen(socketFileDescriptor, 5) != 0) {
        fprintf(stderr, "listen failed: %s\n", strerror(errno));
        close(socketFileDescriptor);
        return -1;
    }

    return socketFileDescriptor;
}

void setupEpollConnection(int epollFileDescriptor, int clientFileDescriptor, struct epoll_event * event) 
{
    event->events = EPOLLIN;
    event->data.fd = clientFileDescriptor;

    epoll_ctl(epollFileDescriptor, EPOLL_CTL_ADD, clientFileDescriptor, event);
}

void strcpyOffset(char *dest, char *src, int offset, int length)
{
	strncpy(dest, src + offset, length);
	dest[length] = '\0';
}

void getUsernameAndPassword(char message[], char username[], char password[])
{
	int usernameFromClientLength = message[1];
	int passwordFromClientLength = message[2];
	
	strcpyOffset(username, message, 3, usernameFromClientLength);
	username[usernameFromClientLength] = '\0';
	
	strcpyOffset(password, message, 3 + usernameFromClientLength, passwordFromClientLength);
	password[passwordFromClientLength] = '\0';
}

void createProblemDatabaseFile()
{
	FILE *problemDatabaseFile = fopen("problems.tsv", "a");
	fclose(problemDatabaseFile);
}


int checkPasswordCompatible(char *password)
{
	int length = strlen(password);
	int containCapital = 0;
	int containSmall = 0;
	int containNumber = 0;
	for(int i = 0; i < length; i++)
	{
		if (password[i] >= 'a' && password[i] <= 'z')
		{
			containSmall = 1;
		}
		else if (password[i] >= 'A' && password[i] <= 'Z')
		{
			containCapital = 1;
		}
		else if (password[i] >= '0' && password[i] <= '9')
		{
			containNumber = 1;
		}
		else
		{
			return 0;
		}
	}
	return containCapital & containSmall & containNumber;
}

int main()
{
	struct sockaddr_in newAddress;
	socklen_t addrlen;

	int serverFileDescriptor;
	int newClientFileDescriptor;
	int currentClientFileDescriptor = -1;
	int temp_fd;
	
	struct epoll_event epollEvents[1024], epoll_temp;
	int epollEvent;
	int epollFileDescriptor = epoll_create(1);
	
	int timeout_msecs = 1000;	
	char message[2048];

	serverFileDescriptor = createServerSocket(); 

	if (serverFileDescriptor == -1) {
		fprintf(stderr, "Failed to create a server\n");
		return -1; 
	}

	setupEpollConnection(epollFileDescriptor, serverFileDescriptor, &epoll_temp);
	
	createProblemDatabaseFile();

	while (1) 
	{
		epollEvent = epoll_wait(epollFileDescriptor, epollEvents, 1024, timeout_msecs);

		if (currentClientFileDescriptor != -1)
		{
			strcpy(message, "0");
			if (send(currentClientFileDescriptor, message, sizeof(message), 0) == -1)
			{
    			epoll_ctl(epollFileDescriptor, EPOLL_CTL_DEL, currentClientFileDescriptor, &epoll_temp);
				close(currentClientFileDescriptor);
				
				currentClientFileDescriptor = -1;
			}
		}

		for (int i = 0; i < epollEvent; i++)
		{
			if (epollEvents[i].data.fd == serverFileDescriptor) 
			{ 
				newClientFileDescriptor = accept(serverFileDescriptor, (struct sockaddr*)&newAddress, &addrlen);

				if (newClientFileDescriptor >= 0) 
				{
					setupEpollConnection(epollFileDescriptor, newClientFileDescriptor, &epoll_temp);
				} 
				else 
				{
					fprintf(stderr, "accept failed [%s]\n", strerror(errno));
				}
			}
			else if (epollEvents[i].events & EPOLLIN) 
			{
				if (epollEvents[i].data.fd < 0) 
				{
					continue;
				}
				
				
				if (recv(epollEvents[i].data.fd, message, 2048, 0)) 
				{
					if (message[0] == 'r')
					{
						FILE *userDatabaseFile = fopen("user.txt", "r");
						if (userDatabaseFile == NULL)
						{
							userDatabaseFile = fopen("user.txt", "w");
							fclose(userDatabaseFile);
							
							userDatabaseFile = fopen("user.txt", "r");
						}
						
						char usernameFromClient[33];
						char passwordFromClient[33];
						getUsernameAndPassword(message, usernameFromClient, passwordFromClient);
						int usernameFromClientLength = strlen(usernameFromClient);
						
						if (strlen(passwordFromClient) >= 6 && checkPasswordCompatible(passwordFromClient) != 0)
						{
							char usernameFromDatabase[128];
							
							char respon[] = "r";
							while(fscanf(userDatabaseFile, " %[^\n]", usernameFromDatabase) != EOF && respon[0] != 'd')
							{
								if (strncmp(usernameFromClient, usernameFromDatabase, usernameFromClientLength) == 0)
								{
									respon[0] = 'd';
								}
							}
							
							fclose(userDatabaseFile);
								
							strcpy(message, respon);
							send(epollEvents[i].data.fd, message, sizeof(message), 0);
							if (respon[0] == 'r')
							{
								userDatabaseFile = fopen("user.txt", "a");
								fprintf(userDatabaseFile, "%s:%s\n", usernameFromClient, passwordFromClient);
							}
						}
						else
						{
							strcpy(message, "d");
							send(epollEvents[i].data.fd, message, sizeof(message), 0);
						}
							
						fclose(userDatabaseFile);
					}
					else if (message[0] == 'l')
					{
						if (currentClientFileDescriptor == -1)
						{
							FILE *userDatabaseFile = fopen("user.txt", "r");
							if (userDatabaseFile == NULL)
							{
								userDatabaseFile = fopen("user.txt", "w");
								fclose(userDatabaseFile);
								strcpy(message, "d");
								send(epollEvents[i].data.fd, message, sizeof(message), 0);
							}
							else
							{
								char usernameFromClient[33];
								char passwordFromClient[33];
								getUsernameAndPassword(message, usernameFromClient, passwordFromClient);
								
								if (strlen(passwordFromClient) >= 6 && checkPasswordCompatible(passwordFromClient) != 0)
								{
									char usernameFromDatabase[33];
									char passwordFromDatabase[33];
									
									char respon[] = "d";
									while(
										fscanf(
											userDatabaseFile, " %[^:]:%[^\n]", usernameFromDatabase, passwordFromDatabase
										) != EOF && respon[0] != 'l'
									)
									{
										if (
											strcmp(usernameFromClient, usernameFromDatabase) == 0 &&
											strcmp(passwordFromClient, passwordFromDatabase) == 0
										)
										{
											respon[0] = 'l';
										}
									}
									
									strcpy(message, respon);
									send(epollEvents[i].data.fd, message, sizeof(message), 0);	
									
									if (respon[0] == 'l')
									{
										currentClientFileDescriptor = epollEvents[i].data.fd;
									}
								}
								else
								{
									strcpy(message, "d");
									send(epollEvents[i].data.fd, message, sizeof(message), 0);
								}
									
								fclose(userDatabaseFile);
							}
						}
						else
						{
							strcpy(message, "u");
							send(epollEvents[i].data.fd, message, sizeof(message), 0);
						}
					}
				}
				}
			}
		}
	

	for (int i = 0; i < 1024; i++) {
		if (epollEvents[i].data.fd > 0) {
			close(epollEvents[i].data.fd);
		}
	}
	
	return 0;
}
