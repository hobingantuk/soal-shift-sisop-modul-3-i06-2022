# soal-shift-sisop-modul-3-I06-2022

I06 Sisop Group 2022

- Yusuf Faiz Shalahudin (5025201119)
- Shafina Chaerunisa (5025201129)
- Bagus Nugraha (5025201162)


## Soal 1

1a. Download the two zip files and unzip them in two different folders with quote name for quote.zip zip file and music for music.zip zip file. Unzip this done simultaneously using threads.

```
strcat(origin, newFile);
    strcpy(destFile, origin);
    strcat(origin, ".zip");

    if(child == 0){
        char *argv[] = {"unzip", "-o", origin, "-d", destFile, NULL};
            execv("/usr/bin/unzip", argv);
```
Through strcat ".zip", it will do unzip both 2 folder (quote and music) in the same time by reading the folder name that has .zip in it.

```
if(pthread_equal(id, tid[0])){
        unzip("quote");
    }
    else if(pthread_equal(id, tid[1])){
        unzip("music");
    }
```
The unzip will use thread and use TID, where we initialize array of 0 and 1 to unzip folder of quote and music.

1b. Decode all existing .txt files with base 64 and put the result in one a new .txt file for each folder (the result will be two .txt files) on the same time using the thread and with the name quote.txt and music.txt. Each sentence is separated by a newline/enter.

Firstly we have to make function where both txt file for each senteced were fixed with enter or new line
```
char text[100] = "echo "" >> ";
        strcat(text, dir);
        execl("/bin/sh", "sh", "-c", text, (char *)0);
```

After that, we decode all txt files with base 64
```
void base64(char* text){
    if(child_id == 0)
        execl("/bin/sh", "sh", "-c", text, (char *)0);
}
```

Lastly, put the decode results into new file of txt with quote.txt and music.txt
```
if(pthread_equal(id, tid[2])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/fina/sisop/modul3/quote/q%d.txt >> /home/fina/sisop/modul3/quote.txt", i);
            base64(cmd);
            newLine("/home/fina/sisop/modul3/quote.txt");
        }
    }
```
```
if(pthread_equal(id, tid[3])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/fina/sisop/modul3/music/m%d.txt >> /home/fina/sisop/modul3/music.txt", i);
            base64(cmd);
            newLine("/home/fina/sisop/modul3/music.txt");
        }
```

1c. Move the two .txt files containing the decoding results to a new folder named results.
This code will make new folder with 'hasil' name
```
if(child_id == 0)
        char *argv[] = {"mkdir", "-p","/home/fina/sisop/modul3/hasil", NULL};
        execv("/usr/bin/mkdir", argv);
```
This code will move file of quote.txt and music.txt into folder 'hasil'
```
if(child_id2 == 0)
            char *argv2[] = {"mv", "/home/fina/sisop/modul3/quote.txt", "/home/fina/sisop/modul3/hasil", NULL};
            execv("/usr/bin/mv", argv2);
else if(child_id3 == 0)
                char *argv3[] = {"mv", "/home/fina/sisop/modul3/music.txt", "/home/fina/sisop/modul3/hasil", NULL};
                execv("/usr/bin/mv", argv3);
```

1d. The results folder is zipped into a results.zip file with the password 'mihinomenest[Name users]'.
```
void hasilZip(char* pass){
    if(child_id == 0)
        char *argv[] = {"zip", "-P", pass, "-r", "hasil.zip", "hasil", NULL};
        execv("/usr/bin/zip", argv);
```

By create zip file of 'hasil', it'll using a password in it. In int main, we can use :
```
    char* username = "fina";
    char pass[100] = "mihinomenestfina";
    strcat(pass, username);
```

Only 1e that we haven't done due to time limitations.


## Soal 2

### client.c
```
void recvWithoutTestByte(int fd, char *buffer, size_t size)
{
	int byteCode = '0';
	while(byteCode == '0')
	{
		recv(fd, buffer, size, 0);
		byteCode = buffer[0];
	}
}
```
This function is used to retrieve the message sent by the server and discard the ping byte sent by the server. The ping byte is sent by the server to check whether the respective user is connected to the server.

And , After that we go to the main.


```
struct sockaddr_in socketAddress;
int socketFileDescriptor;
struct hostent *serverHostData;
char message[2048];

socketFileDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
if (socketFileDescriptor == -1)
{
    fprintf(stderr, "Socket failed: %s\n", strerror(errno));
    return -1;
}

serverHostData = gethostbyname("localhost");
socketAddress.sin_family = AF_INET;
socketAddress.sin_port = htons(8080);
socketAddress.sin_addr = *((struct in_addr *)serverHostData->h_addr);

if (connect(socketFileDescriptor, (struct sockaddr *)&socketAddress, sizeof(struct sockaddr_in)) == -1)
{
    fprintf(stderr, "Socket connection failed: %s\n", strerror(errno));
    close(socketFileDescriptor);
    return -1;
}

int currentPage = 0;
int respon = 0;
char username[33];
char password[33];
char command[2][256];
```
First we create a connection with the server and initialize the variable that needed to store input from user

```
if (currPage == 0)
		{
			while(response <= 0 || response >= 3)
			{
				printf("Pilih menu\n");
				printf("1. Register\n");
				printf("2. Login\n");
				scanf("%d", &response);	
			}
			
			if (response == 1)
			{
				currPage = 1;
			}
			else if (response == 2)
			{
				currPage = 2;
			}
		}
```

currPage == 0 is the home page menu that the user asked to choose between register or login.


```
else if (currPage == 1)
		{
			printf("Register\n");
			printf("Username and password max length is 32\n");
			printf("Username: ");
			scanf("%s", username);
            printf("Password must contain capital, small letter, and numeric\n");
			printf("Password: ");
			scanf("%s", password);
			
			message[0] = 'r';
			message[1] = strlen(username);
			message[2] = strlen(password);
			message[3] = '\0';
			strcat(message, username);
			strcat(message, password);
			
			send(socketFileDescriptor, message, sizeof(message), 0);
			
			recvWithoutTestByte(socketFileDescriptor, message, sizeof(message));
			
			if (message[0] == 'd')
			{
				printf("\nRegister Failed\n\n");
			}
			currentPage = 0;
		}
```
currpage == 1 means a register menu, for account data that being registered is sent using a message with byte r that shows user send register command to the server.

```
else if (currPage == 2)
		{
			
			printf("\nLogin\n");
			printf("Username: ");
			scanf("%s", username);
			printf("Password: ");
			scanf("%s", password);
			
			message[0] = 'l';
			message[1] = strlen(username);
			message[2] = strlen(password);
			message[3] = '\0';
			strcat(message, username);
			strcat(message, password);
			
			send(socketFileDescriptor, message, sizeof(message), 0);
			
			recvWithoutTestByte(socketFileDescriptor, message, sizeof(message));
			
			if (message[0] == 'l')
			{
				currentPage = 3;
			}
			else if (message[0] == 'd')
			{
				printf("\nLogin Failed\n\n");
				currentPage = 0;
			}
			else if (message[0] == 'u')
			{
				printf("\nAnother user still login\n\n");
				currentPage = 0;
			}
		}
	}
```

currPage == 2 is a login menu that has same principles as register menu and the byte in the front message for server is l

### Server.c

```
int createServerSocket() 
{
    struct sockaddr_in socketAddress;
    int socketFileDescriptor;

    socketFileDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); 
    if (socketFileDescriptor == -1) {
        fprintf(stderr, "socket failed: %s\n", strerror(errno));
        return -1;
    }

    socketAddress.sin_family = AF_INET;         
    socketAddress.sin_port = htons(PORT);     
    socketAddress.sin_addr.s_addr = INADDR_ANY; 

    if (bind(socketFileDescriptor, (struct sockaddr *)&socketAddress, sizeof(struct sockaddr_in)) != 0) {
        fprintf(stderr, "bind failed: %s\n", strerror(errno));
        close(socketFileDescriptor);
        return -1;
    }

    if (listen(socketFileDescriptor, 5) != 0) {
        fprintf(stderr, "listen failed: %s\n", strerror(errno));
        close(socketFileDescriptor);
        return -1;
    }

    return socketFileDescriptor;
}
```

This function is a function that create server socket TCP.

```
void setupEpollConnection(int epollFileDescriptor, int clientFileDescriptor, struct epoll_event * event) 
{
    event->events = EPOLLIN;
    event->data.fd = clientFileDescriptor;

    epoll_ctl(epollFileDescriptor, EPOLL_CTL_ADD, clientFileDescriptor, event);
}
```
This is a Function to add user connection descriptor file in epoll.

```
void strcpyOffset(char *dest, char *src, int offset, int length)
{
 strncpy(dest, src + offset, length);
 dest[length] = '\0';
}
```
This function is a modification of strcpy to take a certain subtring length on a string

```
void getUsernameAndPasswordFromClient(char message[], char username[], char password[])
{
 int usernameFromClientLength = message[1];
 int passwordFromClientLength = message[2];
 
 strcpyOffset(username, message, 3, usernameFromClientLength);
 username[usernameFromClientLength] = '\0';
 
 strcpyOffset(password, message, 3 + usernameFromClientLength, passwordFromClientLength);
 password[passwordFromClientLength] = '\0';
}
```
this function used to parse the byte message that sent from user when login/register.

```
void createProblemDatabaseFile()
{
 FILE *problemDatabaseFile = fopen("problems.tsv", "a");
 fclose(problemDatabaseFile);
}

```

Function createProblemDatabaseFile digunakan untuk membuat file database penyimpan problem

Is a function to create file database to save problem




```
int checkPasswordCompatible(char *password)
{
 int length = strlen(password);
 int containCapital = 0;
 int containSmall = 0;
 int containNumber = 0;
 for(int i = 0; i < length; i++)
 {
  if (password[i] >= 'a' && password[i] <= 'z')
  {
   containSmall = 1;
  }
  else if (password[i] >= 'A' && password[i] <= 'Z')
  {
   containCapital = 1;
  }
  else if (password[i] >= '0' && password[i] <= '9')
  {
   containNumber = 1;
  }
  else
  {
   return 0;
  }
 }
 return containCapital & containSmall & containNumber;
}
```

This is a function to check if the string password is same as the requirement.

And then we go to the main.

```
struct sockaddr_in newAddress;
socklen_t addrlen;

int serverFileDescriptor;
int newClientFileDescriptor;
int currentClientFileDescriptor = -1;
int temp_fd;

struct epoll_event epollEvents[1024], epoll_temp;
int epollEvent;
int epollFileDescriptor = epoll_create(1);

int timeout_msecs = 1000; 
char message[2048];

serverFileDescriptor = createTCPServerSocket(); 

if (serverFileDescriptor == -1) {
    fprintf(stderr, "Failed to create a server\n");
    return -1; 
}

setupEpollConnection(epollFileDescriptor, serverFileDescriptor, &epoll_temp);

createProblemDatabaseFile();
```

First Create Server and epoll to store connection and create database.

```
epollEvent = epoll_wait(epollFileDescriptor, epollEvents, 1024, timeout_msecs);

if (currentClientFileDescriptor != -1)
{
    strcpy(message, "0");
    if (send(currentClientFileDescriptor, message, sizeof(message), 0) == -1)
    {
        epoll_ctl(epollFileDescriptor, EPOLL_CTL_DEL, currentClientFileDescriptor, &epoll_temp);
        close(currentClientFileDescriptor);
        
        currentClientFileDescriptor = -1;
    }
}
```
Server will check the message from user and send ping byte to user to check if the user has been discconnected.when it happens, there will be a empty slot for another user to connect.

```
if (message[0] == 'r')
{
    FILE *userDatabaseFile = fopen("user.txt", "r");
    if (userDatabaseFile == NULL)
    {
        userDatabaseFile = fopen("user.txt", "w");
        fclose(userDatabaseFile);
        
        userDatabaseFile = fopen("user.txt", "r");
    }
    
    char usernameFromClient[33];
    char passwordFromClient[33];
    getUsernameAndPasswordFromClient(message, usernameFromClient, passwordFromClient);
    int usernameFromClientLength = strlen(usernameFromClient);
    
    if (strlen(passwordFromClient) >= 6 && checkPasswordCompatible(passwordFromClient) != 0)
    {
        char usernameFromDatabase[128];
        
        char respon[] = "r";
        while(fscanf(userDatabaseFile, " %[^\n]", usernameFromDatabase) != EOF && respon[0] != 'd')
        {
            if (strncmp(usernameFromClient, usernameFromDatabase, usernameFromClientLength) == 0)
            {
                respon[0] = 'd';
            }
        }
        
        fclose(userDatabaseFile);
            
        strcpy(message, respon);
        send(epollEvents[i].data.fd, message, sizeof(message), 0);
        if (respon[0] == 'r')
        {
            userDatabaseFile = fopen("user.txt", "a");
            fprintf(userDatabaseFile, "%s:%s\n", usernameFromClient, passwordFromClient);
        }
    }
    else
    {
        strcpy(message, "d");
        send(epollEvents[i].data.fd, message, sizeof(message), 0);
    }
        
    fclose(userDatabaseFile);
}
```
if byte message r is a sign the user ask for register  and then check if the password and username are both correct. If both correct then server will send 'a' to tell the register is success

```
else if (message[0] == 'l')
{
    if (currentClientFileDescriptor == -1)
    {
        FILE *userDatabaseFile = fopen("user.txt", "r");
        if (userDatabaseFile == NULL)
        {
            userDatabaseFile = fopen("user.txt", "w");
            fclose(userDatabaseFile);
            strcpy(message, "d");
            send(epollEvents[i].data.fd, message, sizeof(message), 0);
        }
        else
        {
            char usernameFromClient[33];
            char passwordFromClient[33];
            getUsernameAndPasswordFromClient(message, usernameFromClient, passwordFromClient);
            
            if (strlen(passwordFromClient) >= 6 && checkPasswordCompatible(passwordFromClient) != 0)
            {
                char usernameFromDatabase[33];
                char passwordFromDatabase[33];
                
                char respon[] = "d";
                while(
                    fscanf(
                        userDatabaseFile, " %[^:]:%[^\n]", usernameFromDatabase, passwordFromDatabase
                    ) != EOF && respon[0] != 'l'
                )
                {
                    if (
                        strcmp(usernameFromClient, usernameFromDatabase) == 0 &&
                        strcmp(passwordFromClient, passwordFromDatabase) == 0
                    )
                    {
                        respon[0] = 'l';
                    }
                }
                
                strcpy(message, respon);
                send(epollEvents[i].data.fd, message, sizeof(message), 0); 
                
                if (respon[0] == 'l')
                {
                    currentClientFileDescriptor = epollEvents[i].data.fd;
                }
            }
            else
            {
                strcpy(message, "d");
                send(epollEvents[i].data.fd, message, sizeof(message), 0);
            }
                
            fclose(userDatabaseFile);
        }
    }
    else
    {
        strcpy(message, "u");
        send(epollEvents[i].data.fd, message, sizeof(message), 0);
    }
}
```

if the byte message from user is L then user tried to login. Username and password that has been inputted by user will being checked by server with their database. If there is another user that has login and not disconnected,then the user cannot login.

## Soal 3






