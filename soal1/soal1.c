#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<wait.h>
#include<pwd.h>
#include<sys/stat.h>
#include<dirent.h>

pthread_t tid[4]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pid_t child;


void unzip(char *newFile){
    int status;
    child = fork();

    char origin[100] = "/home/fina/sisop/modul3/";
    char destFile[100];

    strcat(origin, newFile);
    strcpy(destFile, origin);
    strcat(origin, ".zip");

    if(child == 0){
        char *argv[] = {"unzip", "-o", origin, "-d", destFile, NULL};
            execv("/usr/bin/unzip", argv);
    }
}

void* do_unzip(){
    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[0])){
        unzip("quote");
    }
    else if(pthread_equal(id, tid[1])){
        unzip("music");
    }

    return NULL;
}

void newLine(char* dir){
    int status;
    pid_t child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0){
        char text[100] = "echo "" >> ";
        strcat(text, dir);
        execl("/bin/sh", "sh", "-c", text, (char *)0);
    } 
    else{
        while((wait(&status)) > 0);
    }
}

void base64(char* text){
    int status;
    pid_t child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0){
        execl("/bin/sh", "sh", "-c", text, (char *)0);
    }
    else{
        while((wait(&status)) > 0);
    }
}

void* decode(){
    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[2])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/fina/sisop/modul3/quote/q%d.txt >> /home/fina/sisop/modul3/quote.txt", i);
            base64(cmd);
            newLine("/home/fina/sisop/modul3/quote.txt");
        }
    }
    else if(pthread_equal(id, tid[3])){
        for(int i=1; i<10; i++){
            char cmd[120];
            sprintf(cmd, "base64 -d /home/fina/sisop/modul3/music/m%d.txt >> /home/fina/sisop/modul3/music.txt", i);
            base64(cmd);
            newLine("/home/fina/sisop/modul3/music.txt");
        }
    }
}

void moveFile(){
    int status;
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0){
        char *argv[] = {"mkdir", "-p","/home/fina/sisop/modul3/hasil", NULL};
        execv("/usr/bin/mkdir", argv);
    }
    else{
        while(wait(&status) > 0);
        pid_t child_id2;
        child_id2 = fork();

        if(child_id2 < 0){
            exit(EXIT_FAILURE);
        }
        else if(child_id2 == 0){
            char *argv2[] = {"mv", "/home/fina/sisop/modul3/quote.txt", "/home/fina/sisop/modul3/hasil", NULL};
            execv("/usr/bin/mv", argv2);
        }
        else{
            while(wait(&status) > 0);
            pid_t child_id3;
            child_id3 = fork();

            if(child_id3 < 0){
                exit(EXIT_FAILURE);
            }
            else if(child_id3 == 0){
                char *argv3[] = {"mv", "/home/fina/sisop/modul3/music.txt", "/home/fina/sisop/modul3/hasil", NULL};
                execv("/usr/bin/mv", argv3);
            }
        }
    }
}

void hasilZip(char* pass){
    int status;
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if(child_id == 0){
        char *argv[] = {"zip", "-P", pass, "-r", "hasil.zip", "hasil", NULL};
        execv("/usr/bin/zip", argv);
    }
    else{
        while((wait(&status)) > 0);
    }
}


int main(){

    int status;
    pid_t child_id = fork();
    int err;

    char* username = "fina";
    char pass[100] = "mihinomenestfina";
    strcat(pass, username);


    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if (child_id == 0) {
        for(int i = 0; i < 2; i++) {    
            err = pthread_create(&(tid[i]), NULL, &do_unzip, NULL);
            if(err) {
                printf("\n can't create thread : [%s]",strerror(err));

            } else {
                printf("\n create thread success %d\n", i);
            }
        }
        pthread_join(tid[0],NULL);
        pthread_join(tid[1],NULL);
    }
    else{
            while(wait(&status) > 0);
        
        pid_t child_id2 = fork();

        if(child_id2 < 0){
            exit(EXIT_FAILURE);
        }
        else if(child_id2 == 0){
            for(int i=2; i<4; i++){
                err = pthread_create(&(tid[i]), NULL, &decode, NULL);
                if(err != 0){
                    printf("\n can't create thread : [%s]",strerror(err));
                }
                else{
                    printf("\n create thread success %d\n", i);
                }
            }

            pthread_join(tid[2],NULL);
            pthread_join(tid[3],NULL);
        }
        else{
            while((wait(&status)) > 0);
            moveFile();

            while((wait(&status)) > 0);
            hasilZip(pass);
        }
    }
}